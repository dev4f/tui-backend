# tui-backend

### How it works!

##### Tech

```
Node.js 8.10
PM2
```

##### Install npm dependencies

Go to the application folder and install all the Node.js dependecies

```sh
npm install
```


##### Application

The application is started through process manager PM2, using the file file ecosystem.json.

Logs:

```sh
pm2 logs
```

Status:

```sh
pm2 status
```

Start:

```sh
npm run pm2-start
```

Stop:

```sh
npm run pm2-stop
```


### Features

Personnaly I recommend to use Postman app to test the API endpoint.

Set the following header:

| key          | value |
| ------------ | ----- |
| Accept       | application/json |

Set the following parameter:

| key         | value |
| ----------- | ----- |
| User-Agent  | ${user} |


-   Endpoint

GET: http://127.0.0.1:3008/v1/reposlist

### Tests

```sh
npm test
```

