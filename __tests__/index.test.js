const request = require('supertest');

const app = require('../config');

describe('Endpoints behaviour tests', () => {
  test('Request without or with unsupported header must return 406 status', async () => {
    const response = await request(app).get('/v1/reposlist');
    expect(response.statusCode).toBe(406);
    expect(response.body).toEqual({
      status: 406,
      Message: 'Header not acceptable',
    });
  });
  test('Request with non existent user must return 404 status', async () => {
    const response = await request(app).get('/v1/reposlist').query({ username: 'none' }).set('Accept', 'application/json');
    expect(response.statusCode).toBe(404);
    expect(response.body).toEqual({ Message: 'Not Found', status: 404 });
  });
  test('Request with valid user must return 200 status', async () => {
    const response = await request(app).get('/v1/reposlist').query({ username: 'test' }).set('Accept', 'application/json');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual([
      {
        branches: [
          {
            name: 'master',
            sha: '78180de0cc75ea9b9d823f35f818f824186a5cfa',
          },
        ],
        name: 'CouchSurf',
        owner: 'marmel0',
      },
      {
        branches: [
          {
            name: 'master',
            sha: 'e3fae253bcf34ceca7f757551440af97f3414310',
          },
          {
            name: 'teste',
            sha: 'e3fae253bcf34ceca7f757551440af97f3414310',
          },
        ],
        name: 'producs-api',
        owner: 'marmel0',
      },
      {
        branches: [
          {
            name: 'master',
            sha: 'b4f95cc2690a077c9809deb8836523ed0be6bf9b',
          },
        ],
        name: 'tarefatqs',
        owner: 'marmel0',
      },
    ]);
  });
  test('Request with failure from external api return 500 status', async () => {
    const response = await request(app).get('/v1/reposlist').query({ username: 'error' }).set('Accept', 'application/json');
    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ Message: 'Fake error', status: 500 });
  });
});
