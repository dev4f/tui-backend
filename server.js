const port = process.env.PORT || 3008;
const app = require('./config');

app.listen(port, () => {
  console.log(`Listening to the port: ${port}`);
});
