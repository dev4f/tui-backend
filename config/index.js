const express = require('express');
const bodyParser = require('body-parser');

// Routers instances
const resource = require('../routes');

// Inicializate express framework
const app = express();

app.use(bodyParser.urlencoded({
  extended: true,
}));
// parse application/json
app.use(bodyParser.json());

// routers instanciation
app.use('/v1', resource);

// Export express server so you can import it in the lambda function.
module.exports = app;
