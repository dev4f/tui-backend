
const fakeRepo = require('./data/example-repo');
const fakeBranches = require('./data/example-branch');

module.exports = {
  get: url => ({
    headers: (obj) => {
      if (obj['User-Agent'] === 'error') throw new Error('Fake error');
      if (obj['User-Agent'] !== 'test') return { status: 404, body: 'Not Found' };
      switch (true) {
        case url.includes('CouchSurf'):
          return { body: fakeBranches.CouchSurf };
        case url.includes('tarefatqs'):
          return { body: fakeBranches.tarefatqs };
        case url.includes('producs-api'):
          return { body: fakeBranches['producs-api'] };
        default:
          return {
            status: 200,
            body: fakeRepo,
          };
      }
    },
  }),
};
