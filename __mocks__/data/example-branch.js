module.exports = {
  'producs-api': [{ name: 'master',
    commit:
      { sha: 'e3fae253bcf34ceca7f757551440af97f3414310',
        url: 'https://api.github.com/repos/marmel0/producs-api/commits/e3fae253bcf34ceca7f757551440af97f3414310' },
    protected: false },
  { name: 'teste',
    commit:
      { sha: 'e3fae253bcf34ceca7f757551440af97f3414310',
        url: 'https://api.github.com/repos/marmel0/producs-api/commits/e3fae253bcf34ceca7f757551440af97f3414310' },
    protected: false }],
  tarefatqs: [{ name: 'master',
    commit:
      { sha: 'b4f95cc2690a077c9809deb8836523ed0be6bf9b',
        url: 'https://api.github.com/repos/marmel0/tarefatqs/commits/b4f95cc2690a077c9809deb8836523ed0be6bf9b' },
    protected: false }],
  CouchSurf: [{ name: 'master',
    commit:
      { sha: '78180de0cc75ea9b9d823f35f818f824186a5cfa',
        url: 'https://api.github.com/repos/marmel0/CouchSurf/commits/78180de0cc75ea9b9d823f35f818f824186a5cfa' },
    protected: false }],
};
