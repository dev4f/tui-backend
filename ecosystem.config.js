module.exports = {
  apps: [{
    name: 'backend',
    script: 'server.js',
    // VSCODE debugg attach
    node_args: ['--inspect=7001'],
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '1G',
    env_development_integration: {
      PORT: 3008,
      EXTERNAL_API: 'https://api.github.com',
    },
  }],
};
