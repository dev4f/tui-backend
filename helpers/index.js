const unirest = require('unirest');

const externalUrl = process.env.EXTERNAL_API || 'https://api.github.com';

const getDetails = async (url, username) => unirest.get(url)
  .headers({ 'User-Agent': username, Accept: 'application/json' });

const objExtracter = braches => Promise.all(
  braches.map(async (branch) => {
    const { name, commit } = branch;
    return {
      name,
      sha: commit.sha,
    };
  }),
);

const responseBuilder = async (repos, username) => Promise.all(repos
  .filter(repo => !repo.fork)
  .map(async (repo) => {
    const { name, owner } = repo;
    const { body } = await getDetails(`${externalUrl}/repos/${username}/${name}/branches`, username);
    const branchDetails = await objExtracter(body);
    return {
      name,
      owner: owner.login,
      branches: branchDetails,
    };
  }));

module.exports = {
  getDetails,
  responseBuilder,
};
