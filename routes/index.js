const router = require('express').Router();

const { getDetails, responseBuilder } = require('../helpers');

const externalUrl = process.env.EXTERNAL_API || 'https://api.github.com';
const CustomError = require('../response');

router.get('/reposlist', async (req, res) => {
  const { username } = req.query;
  const { accept } = req.headers;

  if (!accept || accept !== 'application/json') {
    // errorMsg(res, 406, 'Header not acceptable');
    res.status(406).json(new CustomError(406, 'Header not acceptable'));
    return;
  }

  try {
    const { body, status } = await getDetails(`${externalUrl}/users/${username}/repos`, username);
    if (status !== 200) {
      // errorMsg(res, status, body.message || body);
      res.status(status).json(new CustomError(status, body.message || body));
      return;
    }

    const responseUser = await responseBuilder(body, username);
    res.status(200).json(responseUser);
  } catch (error) {
    res.status(500).json(new CustomError(500, error.message || 'Something went wrong'));
    // errorMsg(res, 500, error.message || 'Something went wrong');
  }
});

module.exports = router;
